import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.scss']
})
export class AddItemComponent implements OnInit {
addItemForm: FormGroup;
error: Observable<Error>;
  constructor(private builder: FormBuilder) {
    this.addItemForm = this.builder.group({
      name: this.builder.control(' ', Validators.required),
      isActive: [false]

    });
   }

  ngOnInit(): void {
  }
  onSubmit(): void {
    if (this.addItemForm.valid) {
    }
  }

}
