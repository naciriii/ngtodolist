import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddItemComponent } from './add-item/add-item.component';

import { ItemsComponent } from './items.component';

const routes: Routes = [{ path: '', component: ItemsComponent },
{
  path: 'add',
  component: AddItemComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ItemsRoutingModule { }
