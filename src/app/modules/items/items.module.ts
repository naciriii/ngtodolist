import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ItemsRoutingModule } from './items-routing.module';
import { ItemsComponent } from './items.component';
import { ListItemsComponent } from './list-items/list-items.component';
import { AddItemComponent } from './add-item/add-item.component';
import { ReactiveFormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import * as fromItemStore from './state/reducers';
import * as fromItemEffects from '@modules/items/state/effects';
import { EffectsModule } from '@ngrx/effects';


@NgModule({
  declarations: [ItemsComponent, ListItemsComponent, AddItemComponent],
  imports: [
    CommonModule,
    ItemsRoutingModule,
    ReactiveFormsModule,
    StoreModule.forFeature('itemState', fromItemStore.reducers.itemState),
    EffectsModule.forFeature(fromItemEffects.effects)
  ]
})
export class ItemsModule { }
