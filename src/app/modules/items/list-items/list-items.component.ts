import { Component, OnInit } from '@angular/core';
import AppState from '@app/state/models';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Item } from '../models/item';
import { LoadItemsAction } from '../state/actions';

@Component({
  selector: 'app-list-items',
  templateUrl: './list-items.component.html',
  styleUrls: ['./list-items.component.scss']
})
export class ListItemsComponent implements OnInit {
  items: Observable<Array<Item>>;
  error: Observable<Error>;
  isLoading: Observable<boolean>;
  constructor(private state: Store<AppState>) {
  }

  ngOnInit(): void {
    this.state.dispatch(new LoadItemsAction(true));

    this.items = this.state.select(store => store.itemState.items);
    this.isLoading = this.state.select(store => store.itemState.isLoading);

    this.error = this.state.select(store => store.itemState.hasError);



  }
  onDelete(id: number): void {

  }
  getIndex(item, index): number {
    return index;
  }

}
