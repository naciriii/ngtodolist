export interface Item {
    id: number;
    name: string;
    isActive: boolean;
}
