import { Item } from '@modules/items/models/item';
import { Action } from '@ngrx/store';
import { ItemState } from '../models/ItemState';


export enum ItemActionTypes {
    LOAD_ITEMS_ACTION = '[Items] LOAD_ITEMS_ACTION',
    LOAD_ITEMS_SUCCESS_ACTION = '[Items] LOAD_ITEMS_SUCCESS_ACTION',
    LOAD_ITEMS_FAILURE_ACTION = '[Items] LOAD_ITEMS_FAILURE',
}
abstract class Actionable implements Action {
    readonly type;
    constructor(public payload: any) {


    }
}
export class LoadItemsAction extends Actionable {
    readonly type = ItemActionTypes.LOAD_ITEMS_ACTION;
}
export class LoadItemsSuccessAction extends Actionable {
    readonly type = ItemActionTypes.LOAD_ITEMS_SUCCESS_ACTION;
}
export class LoadItemsFailureAction extends Actionable {
    readonly type = ItemActionTypes.LOAD_ITEMS_FAILURE_ACTION;
}

export type ItemAction = Actionable;
