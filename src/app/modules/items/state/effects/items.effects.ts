import { Injectable } from '@angular/core';
import { ItemsService } from '@modules/items/services/items.service';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, mergeMap, switchMap } from 'rxjs/operators';
import { ItemActionTypes, LoadItemsAction, LoadItemsFailureAction, LoadItemsSuccessAction } from '../actions';

@Injectable()
export class ItemEffects  {

    constructor(private actions: Actions, private itemsService: ItemsService) {


    }
    @Effect() loadItems =
        this.actions.pipe(
            ofType<LoadItemsAction>(ItemActionTypes.LOAD_ITEMS_ACTION),
            mergeMap((actions) => {
                 return this.itemsService.getItems().pipe(map(data => {
                    return new LoadItemsSuccessAction(data);
                }));
            })
        );



}
export const effects = [ItemEffects];
