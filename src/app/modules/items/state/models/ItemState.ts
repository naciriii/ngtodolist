import { Item } from '@modules/items/models/item';

export interface ItemState {
    items: Array<Item>;
    isLoading: boolean;
    hasError: Error;


}
