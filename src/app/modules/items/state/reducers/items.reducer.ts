import { ItemAction, ItemActionTypes } from '../actions';
import { ItemState } from '../models/ItemState';

const initialState: ItemState = {
    items: [],
    isLoading: false,
    hasError: null

};

export function ItemsReducer(state: ItemState = initialState,
                             action: ItemAction): ItemState {
    switch (action.type) {
        case ItemActionTypes.LOAD_ITEMS_ACTION:
            return { ...state, isLoading: true };
        case ItemActionTypes.LOAD_ITEMS_SUCCESS_ACTION:
            return { ...state, items: action.payload, isLoading: false };
        case ItemActionTypes.LOAD_ITEMS_FAILURE_ACTION:
            return { ...state, hasError: action.payload, isLoading: false };

        default:
            return state;


    }
}
export const reducers = { itemState: ItemsReducer };
