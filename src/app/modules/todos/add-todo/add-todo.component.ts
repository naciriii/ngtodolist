import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import AppState from '@app/state/models';
import { Todo } from '../models/todo';
import { AddTodoAction } from '@modules/todos/state/actions/todo.actions';

@Component({
  selector: 'app-add-todo',
  templateUrl: './add-todo.component.html',
  styleUrls: ['./add-todo.component.scss']
})
export class AddTodoComponent implements OnInit {
  addTodoForm: FormGroup;
  error: Observable<Error>;

  constructor(private builder: FormBuilder,
              private router: Router,
              private store: Store<AppState>
  ) {
    this.addTodoForm = this.builder.group({
      name: this.builder.control('', Validators.required),
      description: this.builder.control(' ', [Validators.required, Validators.minLength(10)])
    });
  }

  ngOnInit(): void {
    this.error = this.store.select(store => store.todoState.hasError);

  }
  onSubmit(): Observable<Todo> | boolean {
    if (!this.addTodoForm.valid) { return false; }
    this.store.dispatch(new AddTodoAction(this.addTodoForm.value));
    this.error.subscribe(err => {
      if (!err) {
        this.router.navigate(['todos']);
      }
    });








  }

}
