import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AddTodoAction } from '@modules/todos/state/actions/todo.actions';
import { Todo } from '../models/todo';
import { TodoService } from '../services/todo.service';
import AppState from '@app/state/models';

@Component({
  selector: 'app-add-todo',
  templateUrl: './add-todo.component.html',
  styleUrls: ['./add-todo.component.scss']
})
export class AddTodoComponent implements OnInit {
addTodoForm: FormGroup;

  constructor(private builder: FormBuilder, private todoService: TodoService,
    private router:Router,
    private store:Store<AppState>) {
    this.addTodoForm = this.builder.group({
      name: this.builder.control('', Validators.required),
      description: this.builder.control(' ', [Validators.required, Validators.minLength(10)])
    });
   }

  ngOnInit(): void {
  }
  onSubmit(): Observable<Todo> | boolean {
    if (!this.addTodoForm.valid) { return false; }
    this.todoService.postTodo(this.addTodoForm.value).subscribe(todo => {
      this.store.dispatch(new AddTodoAction(todo))
      this.router.navigate(['todos'])

    },
    error => {

    });


  }

}
