import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import AppState from '@app/state/models';
import { Todo } from '../models/todo';
import { TodoService } from '../services/todo.service';
import { UpdateTodoAction } from '../state/actions/todo.actions';

@Component({
  selector: 'app-edit-todo',
  templateUrl: './edit-todo.component.html',
  styleUrls: ['./edit-todo.component.scss']
})
export class EditTodoComponent implements OnInit {
  editTodoForm: FormGroup;
  todo: Todo;
  error: Observable<Error>;
  constructor(private activeRoute: ActivatedRoute,
              private router: Router,
              private builder: FormBuilder,
              private todoService: TodoService,
              private store: Store<AppState>) {
    this.editTodoForm = this.builder.group({
      name: this.builder.control('', Validators.required),
      description: this.builder.control(' ', [Validators.required, Validators.minLength(10)])
    });
  }

  ngOnInit(): void {
    this.error = this.store.select(store => store.todoState.hasError);
    const id: number = this.activeRoute.snapshot.params.id;
    this.todoService.getTodo(id).subscribe(todo => {
      this.todo = todo;
      this.editTodoForm.patchValue(todo);
    });



  }
  onSubmit(): void {
    this.store.dispatch(
      new UpdateTodoAction({ id: this.todo.id, ...this.editTodoForm.value }));
    this.error.subscribe(err => {
      if (!err) {
        this.router.navigate(['todos']);

      }
    });
  }





}
