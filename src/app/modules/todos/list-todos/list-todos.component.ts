import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { DeleteTodoAction } from '../state/actions/todo.actions';
import { Todo } from '../models/todo';
import { TodoService } from '../services/todo.service';
import AppState from '@app/state/models';

@Component({
  selector: 'app-list-todos',
  templateUrl: './list-todos.component.html',
  styleUrls: ['./list-todos.component.scss']
})
export class ListTodosComponent implements OnInit {
  todos: Observable<Todo[]>;
  loading: Observable<boolean>;
  error: Observable<Error>;

  constructor(private store: Store<AppState>) { }

  ngOnInit(): void {
    this.loading = this.store.select(store => store.todoState.isLoading);
    this.todos = this.store.select(store => store.todoState.todos);
    this.error = this.store.select(store => store.todoState.hasError);

  }

  onDelete(id: number): void {

    this.store.dispatch(new DeleteTodoAction(id));


  }
  getIndex(item, index): number {
    return index;
  }

}
