import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Todo } from '../models/todo';
import {delay} from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  static apiUrl = environment.apiUrl + '/todos';
  constructor(private http: HttpClient) {

   }
   getTodos(): Observable<Todo[]>{
     return this.http.get<Todo[]>(TodoService.apiUrl).pipe(delay(500));
   }
   getTodo(id: number): Observable<Todo> {
     return this.http.get<Todo>(TodoService.apiUrl + '/' + id).pipe(delay(500));
   }
   postTodo(todo: Todo): Observable<Todo> {
     return this.http.post<Todo>(TodoService.apiUrl, todo).pipe(delay(500));
   }
   deleteTodo(id: number): Observable<Todo> {
     return this.http.delete<Todo>(TodoService.apiUrl + '/' + id).pipe(delay(500));
   }
   updateTodo(todo: Todo): Observable<Todo> {
     return this.http.patch<Todo>(TodoService.apiUrl + '/' + todo.id, todo).pipe(delay(500));
   }

}
