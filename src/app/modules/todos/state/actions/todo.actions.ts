import { Action } from '@ngrx/store';

export enum TodoActionTypes {
    LOAD_TODOS = '[TODO] Load todos',
    ADD_TODO = '[TODO] Add todo',
    DELETE_TODO = '[TODO] Delete todo',
    UPDATE_TODO = '[TODO]Update todo',
    LOAD_TODOS_SUCCESS = '[TODO] Load todos success',
    ADD_TODO_SUCCESS = '[TODO] Add todo success',
    DELETE_TODO_SUCCESS = '[TODO] Delete todo success',
    UPDATE_TODO_SUCCESS = '[TODO]Update todo success',
    LOAD_TODOS_FAILURE = '[TODO] Load todos failure',
    ADD_TODO_FAILURE = '[TODO] Add todo failure',
    DELETE_TODO_FAILURE = '[TODO] Delete todo failure',
    UPDATE_TODO_FAILURE = '[TODO]Update todo failure'
}


export abstract class Actionable implements Action {
    readonly type;
    constructor(public payload: any) {

    }

}
export class LoadTodosAction extends Actionable {
    type = TodoActionTypes.LOAD_TODOS;

}

export class AddTodoAction extends Actionable {
    type = TodoActionTypes.ADD_TODO;

}
export class DeleteTodoAction extends Actionable {
    type = TodoActionTypes.DELETE_TODO;

}

export class UpdateTodoAction extends Actionable {
    type = TodoActionTypes.UPDATE_TODO;
}
// ----- Failures
export class LoadTodosFailureAction extends Actionable {
    type = TodoActionTypes.LOAD_TODOS_FAILURE;

}
export class AddTodoFailureAction extends Actionable {
    type = TodoActionTypes.ADD_TODO_FAILURE;

}
export class DeleteTodoFailureAction extends Actionable {
    type = TodoActionTypes.DELETE_TODO_FAILURE;

}
export class UpdateTodoFailureAction extends Actionable {
    type = TodoActionTypes.UPDATE_TODO_FAILURE;
}
// -- Success
export class LoadTodosSuccessAction extends Actionable {
    type = TodoActionTypes.LOAD_TODOS_SUCCESS;

}
export class AddTodoSuccessAction extends Actionable {
    type = TodoActionTypes.ADD_TODO_SUCCESS;

}
export class DeleteTodoSuccessAction extends Actionable {
    type = TodoActionTypes.DELETE_TODO_SUCCESS;

}
export class UpdateTodoSuccessAction extends Actionable {
    type = TodoActionTypes.UPDATE_TODO_SUCCESS;
}



