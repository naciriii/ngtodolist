import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, concatMap, map, mergeMap, switchMap } from 'rxjs/operators';
import { TodoService } from '../../services/todo.service';
import * as TodoActions from '../actions/todo.actions';

@Injectable()
export class TodoEffects {
    constructor(private actions: Actions, private todoService: TodoService) {

    }


   @Effect() loadTodos = this.actions
   .pipe(ofType(TodoActions.TodoActionTypes.LOAD_TODOS),
   mergeMap(actions => {

       return this.todoService.getTodos().pipe(map(data => {
           return  new TodoActions.LoadTodosSuccessAction(data);

   }), catchError(err => of(new TodoActions.LoadTodosFailureAction(err)))
   ); } )
   );

    @Effect() addTodo = this.actions
        .pipe(ofType<TodoActions.AddTodoAction>(TodoActions.TodoActionTypes.ADD_TODO),
            mergeMap(
                (action) => {
                    return this.todoService.postTodo(action.payload)
                        .pipe(
                            map(data => new TodoActions.AddTodoSuccessAction(data)),
                            catchError(error => {
                                return of(new TodoActions.AddTodoFailureAction(error)
                                );
                            }));
                }));
    @Effect() deleteTodo = this.actions
        .pipe(ofType<TodoActions.DeleteTodoAction>(TodoActions.TodoActionTypes.DELETE_TODO),
            mergeMap(
                (action) => {
                    return this.todoService.deleteTodo(action.payload)
                        .pipe(
                            map(data => new TodoActions.DeleteTodoSuccessAction(action.payload)),
                            catchError(error => {
                                return of(new TodoActions.DeleteTodoFailureAction(error)
                                );
                            }));
                }));

    @Effect() updateTodo = this.actions
        .pipe(ofType<TodoActions.UpdateTodoAction>(TodoActions.TodoActionTypes.UPDATE_TODO),
            concatMap((action) => {
                return this.todoService.updateTodo(action.payload).
                    pipe(
                        map(data => {
                            return new TodoActions.UpdateTodoSuccessAction(data);
                        }), catchError(err => of(new TodoActions.DeleteTodoFailureAction(err)))

                    );
            }
            )
        );

    @Effect() init = () => of(new TodoActions.LoadTodosAction(true));
}





