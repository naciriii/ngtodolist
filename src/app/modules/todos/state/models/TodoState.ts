import { Todo } from '../../models/todo';

export interface TodoState {
    todos: Array<Todo>;
    isLoading: boolean;
    hasError: Error;
}
