import { Actionable as TodoAction, TodoActionTypes } from '../actions/todo.actions';
import { TodoState } from '@modules/todos/state/models/TodoState';

const initialState: TodoState = {
    todos: [],
    isLoading: false,
    hasError: null
};

export function TodoReducer(state: TodoState = initialState, action: TodoAction): TodoState {
    switch (action.type) {
        // loading
        case TodoActionTypes.LOAD_TODOS:
            return { ...state, isLoading: true };
        case TodoActionTypes.ADD_TODO:
            return { ...state, isLoading: true };
        case TodoActionTypes.UPDATE_TODO:
            return { ...state, isLoading: true };
        case TodoActionTypes.DELETE_TODO:
            return { ...state, isLoading: true };
        // --failure
        case TodoActionTypes.LOAD_TODOS_FAILURE:
            return { ...state, isLoading: false, hasError: action.payload };
        case TodoActionTypes.ADD_TODO_FAILURE:
            return { ...state, isLoading: false, hasError: action.payload };
        case TodoActionTypes.UPDATE_TODO_FAILURE:
            return { ...state, isLoading: false, hasError: action.payload };
        case TodoActionTypes.DELETE_TODO_FAILURE:
            return { ...state, isLoading: false, hasError: action.payload };

        // failure

        // success
        case TodoActionTypes.LOAD_TODOS_SUCCESS:

            return { ...state, todos: action.payload, isLoading: false };

        case TodoActionTypes.ADD_TODO_SUCCESS:
            return { ...state, todos: [...state.todos, action.payload], isLoading: false };

        case TodoActionTypes.DELETE_TODO_SUCCESS:
            return { ...state, todos: state.todos.filter(item => item.id !== action.payload), isLoading: false };
        case TodoActionTypes.UPDATE_TODO_SUCCESS:

            return {
                ...state,
                todos: [...state.todos.filter(item => item.id !== action.payload.id), action.payload]
                , isLoading: false
            };


        default:
            return state;
    }
}
