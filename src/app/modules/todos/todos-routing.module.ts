import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddTodoComponent } from './add-todo/add-todo.component';
import { EditTodoComponent } from './edit-todo/edit-todo.component';

import { TodosComponent } from './todos.component';

const routes: Routes = [{ path: '', component: TodosComponent }
, {
  path: 'add',
  component: AddTodoComponent
},
{path: 'edit/:id',
component: EditTodoComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TodosRoutingModule { }
