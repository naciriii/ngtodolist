import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TodosRoutingModule } from './todos-routing.module';
import { TodosComponent } from './todos.component';
import { AddTodoComponent } from './add-todo/add-todo.component';
import { ListTodosComponent } from './list-todos/list-todos.component';
import { ReactiveFormsModule } from '@angular/forms';
import { EditTodoComponent } from './edit-todo/edit-todo.component';
import { StoreModule } from '@ngrx/store';
import * as fromState from './state';
import { EffectsModule } from '@ngrx/effects';


@NgModule({
  declarations: [TodosComponent, AddTodoComponent, ListTodosComponent, EditTodoComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    TodosRoutingModule,
    StoreModule.forFeature('todoState', fromState.TodoReducer),
    EffectsModule.forFeature([fromState.TodoEffects]),
  ]
})
export class TodosModule { }
