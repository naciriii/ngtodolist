import { ItemState } from '@modules/items/state/models/ItemState';
import { TodoState } from '@modules/todos/state/models';

export interface AppState {
    todoState: TodoState;
    itemState: ItemState;
}
